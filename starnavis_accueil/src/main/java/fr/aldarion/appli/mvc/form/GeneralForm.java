/**
 * 
 */
package fr.aldarion.appli.mvc.form;

import java.io.Serializable;
import java.util.Locale;

import fr.aldarion.appli.beans.application.Application;
import fr.aldarion.appli.utils.Constantes;

/**
 * @author cedric
 *
 */
public class GeneralForm implements Serializable, Constantes {

	/** serial */
	private static final long serialVersionUID = 1L;

	/** Information de l'application */
	private Application		  application;

	/** Titre de la page */
	private String			  titrePage;

	/** Langue de la page */
	private Locale			  langue;

	/**
	 * @return the application
	 */
	public Application getApplication() {
		return application;
	}

	/**
	 * @param application
	 *            the application to set
	 */
	public void setApplication(Application application) {
		this.application = application;
	}

	/**
	 * @return the titrePage
	 */
	public String getTitrePage() {
		return titrePage;
	}

	/**
	 * @param titrePage
	 *            the titrePage to set
	 */
	public void setTitrePage(String titrePage) {
		this.titrePage = titrePage;
	}

	/**
	 * @return the langue
	 */
	public Locale getLangue() {
		return langue;
	}

	/**
	 * @param langue
	 *            the langue to set
	 */
	public void setLangue(Locale langue) {
		this.langue = langue;
	}

}
