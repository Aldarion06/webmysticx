package fr.aldarion.appli.mvc.form.accueil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fr.aldarion.appli.beans.Nomenclature;
import fr.aldarion.appli.mvc.form.GeneralForm;

/**
 * 
 * @author cedric
 *
 */
public class AccueilForm extends GeneralForm implements Serializable {

	/** serial */
	private static final long  serialVersionUID	= 1L;

	/** Menu généré pour page accueil */
	private List<Nomenclature> elementsMenu		= new ArrayList<Nomenclature>();

	/** Liste des slides de la page d'accueil */
	private List<Nomenclature> slides			= new ArrayList<Nomenclature>();

	/**
	 * @return the elementsMenu
	 */
	public List<Nomenclature> getElementsMenu() {
		return elementsMenu;
	}

	/**
	 * @param elementsMenu
	 *            the elementsMenu to set
	 */
	public void setElementsMenu(List<Nomenclature> elementsMenu) {
		this.elementsMenu = elementsMenu;
	}

	/**
	 * @return the slides
	 */
	public List<Nomenclature> getSlides() {
		return slides;
	}

	/**
	 * @param slides the slides to set
	 */
	public void setSlides(List<Nomenclature> slides) {
		this.slides = slides;
	}

}
