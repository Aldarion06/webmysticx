/**
 * 
 */
package fr.aldarion.appli.mvc.actions.accueil;

import java.util.List;
import java.util.Locale;

import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import fr.aldarion.appli.beans.Nomenclature;
import fr.aldarion.appli.mvc.actions.GeneralFormAction;
import fr.aldarion.appli.mvc.form.accueil.AccueilForm;
import fr.aldarion.appli.service.GestionLangageService;

/**
 * @author cedric
 *
 */
public class AccueilFormAction extends GeneralFormAction {

	/** Mon Objet Form */
	private AccueilForm			  monObjetFormulaire;

	/** Gestionnaire de langage */
	private GestionLangageService gestionlangageserviceso;

	/** Nom de la page */
	private static final String	  NOM_PAGE = "Accueil";

	/**
	 * Méthode appelé à chaque page
	 */
	@Override
	public Event setupForm(RequestContext context) throws Exception {

		super.setupForm(context);

		if (logger.isInfoEnabled()) {
			logger.info("Initialisation page Accueil");
		}
		// ---------------------------------------------
		// Récupération / Initialisation des variables
		// ---------------------------------------------
		monObjetFormulaire = (AccueilForm) context.getFlowScope().get(getFormObjectName());

		// ---------------------------------------------
		// Valorisation des éléments commun de la page
		// ---------------------------------------------

		// Titre de la page
		monObjetFormulaire.setTitrePage(monObjetFormulaire.getTitrePage() + NOM_PAGE);

		return success();
	}

	/**
	 * Méthode d'initialisation du menu
	 * 
	 * @param context
	 * @return
	 */
	public Event initialiserElementPage(RequestContext context) {

		// Récupération du menu en fonction de la langue
		monObjetFormulaire.setElementsMenu(this.retournerMenuGenere(monObjetFormulaire.getLangue()));
		monObjetFormulaire.setSlides(this.retournerSlides(monObjetFormulaire.getLangue()));

		return success();
	}

	/**
	 * Méthode retournant le menu en fonction de la langue
	 * 
	 * @return
	 */
	private List<Nomenclature> retournerMenuGenere(Locale locale) {

		if (logger.isInfoEnabled()) {
			logger.info("Génération du menu");
		}

		return gestionlangageserviceso.genererMenu(locale);

	}

	/**
	 * Méthode permettant d'alimenter les slides de la page d'accueil
	 * 
	 * @return
	 */
	private List<Nomenclature> retournerSlides(Locale locale) {

		if (logger.isInfoEnabled()) {
			logger.info("Génération des slides");
		}

		return gestionlangageserviceso.genererSlides(locale);

	}

	/**
	 * @return the gestionlangageserviceso
	 */
	public GestionLangageService getGestionlangageserviceso() {
		return gestionlangageserviceso;
	}

	/**
	 * @param gestionlangageserviceso
	 *            the gestionlangageserviceso to set
	 */
	public void setGestionlangageserviceso(GestionLangageService gestionlangageserviceso) {
		this.gestionlangageserviceso = gestionlangageserviceso;
	}

}
