/**
 * 
 */
package fr.aldarion.appli.mvc.actions;

import java.util.Locale;

import org.apache.log4j.Logger;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import fr.aldarion.appli.beans.application.Application;
import fr.aldarion.appli.mvc.form.GeneralForm;
import fr.aldarion.appli.mvc.form.accueil.AccueilForm;

/**
 * @author cedric
 *
 */
public class GeneralFormAction extends FormAction {

	/** Mon Objet formulaire */
	protected GeneralForm	monObjetFormulaire;

	/** Logger */
	protected static Logger	logger = Logger.getLogger(GeneralFormAction.class);

	/** Clé d'accès pour la langue */
	private static String	LANGUE = "langue";

	/** Initialisation du formulaire */
	@Override
	public Event setupForm(RequestContext context) throws Exception {

		super.setupForm(context);

		if (logger.isInfoEnabled()) {
			logger.info("Mode debug activé");
			logger.info("Lancement de l'application");
		}

		// ---------------------------------------------
		// Récupération / Initialisation des variables
		// ---------------------------------------------

		monObjetFormulaire = (AccueilForm) context.getFlowScope().get(getFormObjectName());

		// ---------------------------------------------
		// Valorisation des éléments commun de la page
		// ---------------------------------------------

		// Application
		monObjetFormulaire.setApplication(new Application());

		// Début titre page
		monObjetFormulaire.setTitrePage(monObjetFormulaire.getApplication().getLibelle() + "  "
		        + monObjetFormulaire.getApplication().getVersion() + " - ");

		return success();
	}

	/**
	 * Méthode d'initialisation de la langue du site
	 * 
	 * @param context
	 * @return EVENT
	 */
	public Event initialiserLangueSite(RequestContext context) {

		if (logger.isInfoEnabled()) {
			logger.info("Initialisation languel");
		}

		// Mise en place de la langue
		String language = context.getRequestParameters().get(LANGUE);
		monObjetFormulaire.setLangue(this.retournerLocaleEnFonctionLangue(language));

		return success();
	}

	/** Méthode permettant de retourner l'objet locale */
	private Locale retournerLocaleEnFonctionLangue(String langue) {

		if (langue != null && langue.equalsIgnoreCase("en")) {
			return new Locale("en", "EN");
		}

		return new Locale("fr", "");

	}
}
