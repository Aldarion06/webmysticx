/**
 * 
 */
package fr.aldarion.appli.service;

import java.util.List;
import java.util.Locale;

import fr.aldarion.appli.beans.Nomenclature;

/**
 * @author cedric
 *
 */
public interface GestionLangageService extends GestionService {

	/** Menu - Accueil */
	String MENU_ACCUEIL		 = null;

	/** Menu - Comment jouer */
	String MENU_COMMENTJOUER = null;

	/** Menu - Règlement */
	String MENU_REGLEMENT	 = null;

	/** Menu - Forum */
	String MENU_FORUM		 = null;

	/** Menu - ENREGISTRER */
	String MENU_ENREGISTRER	 = null;

	/** Menu - CONNEXION */
	String MENU_CONNEXION	 = null;

	/** Slide 1 - NOVUS */
	String SLIDE_1			 = null;

	/** Slide 1 - EMPEREUR */
	String SLIDE_2			 = null;

	/** Méthode permettant générant un menu en fonction du langage */
	abstract List<Nomenclature> genererMenu(Locale locale);

	/** Méthode permettant générant les slides en fonction du langage */
	abstract List<Nomenclature> genererSlides(Locale locale);

}
