/**
 * 
 */
package fr.aldarion.appli.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.context.MessageSource;

import fr.aldarion.appli.beans.Nomenclature;
import fr.aldarion.appli.beans.application.Langue;
import fr.aldarion.appli.service.GestionLangageService;

/**
 * @author cedric
 *
 */
public class GestionLangageServiceImpl implements GestionLangageService {

	/** Gestionnaire messageSource */
	private MessageSource messageSource;

	/** Paramètre de langue */
	private Langue		  langue;

	public List<Nomenclature> genererMenu(Locale locale) {
		List<Nomenclature> menu = new ArrayList<Nomenclature>();

		menu.add(new Nomenclature(LIEN_ACCUEIL, messageSource.getMessage("menu.accueil", null, locale)));
		menu.add(new Nomenclature(LIEN_COMMENT_JOUEUR, messageSource.getMessage("menu.commentjouer", null, locale)));
		menu.add(new Nomenclature(LIEN_REGLEMENT, messageSource.getMessage("menu.reglement", null, locale)));
		menu.add(new Nomenclature(LIEN_FORUM, messageSource.getMessage("menu.forum", null, locale)));
		menu.add(new Nomenclature(LIEN_ENREGISTRER, messageSource.getMessage("menu.enregistrer", null, locale)));
		menu.add(new Nomenclature(LIEN_CONNECTER, messageSource.getMessage("menu.connexion", null, locale)));

		return menu;

	}

	public List<Nomenclature> genererSlides(Locale locale) {
		List<Nomenclature> Slides = new ArrayList<Nomenclature>();

		Slides.add(new Nomenclature(IMAGE_SLIDE_1, messageSource.getMessage("slide.novus", null, locale)));
		Slides.add(new Nomenclature(IMAGE_SLIDE_2, messageSource.getMessage("slide.empereur", null, locale)));

		return Slides;
	}

	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource
	 *            the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * @return the langue
	 */
	public Langue getLangue() {
		return langue;
	}

	/**
	 * @param langue
	 *            the langue to set
	 */
	public void setLangue(Langue langue) {
		this.langue = langue;
	}

}
