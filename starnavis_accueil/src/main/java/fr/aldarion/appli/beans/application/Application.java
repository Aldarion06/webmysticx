/**
 * 
 */
package fr.aldarion.appli.beans.application;

import fr.aldarion.appli.beans.Entite;
import fr.aldarion.appli.utils.Constantes;

/**
 * @author cedric
 *
 */
public class Application extends Entite implements Constantes {

	/** Serial */
	private static final long	serialVersionUID = 1L;

	/** Version de l'application */
	private final static String	VERSION			 = APPLI_VERSION;

	/** Nom de l'application */
	private final static String	NOM				 = APPLI_NOM;

	/** Description de l'application */
	private String				description		 = APPLI_DESC;

	private String				motCles			 = APPLI_KEYWORDS;

	/** Constructeur sans argument */
	public Application() {
		super(NOM, VERSION);

	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the motCles
	 */
	public String getMotCles() {
		return motCles;
	}

	/**
	 * @param motCles
	 *            the motCles to set
	 */
	public void setMotCles(String motCles) {
		this.motCles = motCles;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return VERSION;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return NOM;
	}

}
