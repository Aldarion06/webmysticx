/**
 * 
 */
package fr.aldarion.appli.beans;

import java.io.Serializable;

/**
 * @author cedric
 *
 */
public class Nomenclature implements Serializable {

	/** Serial */
	private static final long serialVersionUID = 1L;

	/** Identifiant */
	private String			  id;

	/** Valeur */
	private String			  value;

	/** Constructeur dans argument */
	public Nomenclature() {

	};

	/**
	 * @param id
	 * @param value
	 */
	public Nomenclature(String id, String value) {
		super();
		this.id = id;
		this.value = value;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
