/**
 * Entité mère de l'ensemble des beans métiers
 */
package fr.aldarion.appli.beans;

import java.io.Serializable;

/**
 * @author cedric
 *
 */
public class Entite implements Serializable {

	/** Serial */
	private static final long serialVersionUID = 1L;

	/** Identifiant */
	private int				  id;

	/** Libelle */
	private String			  libelle;

	/** Version */
	private String			  version;

	/** Date MAJ */
	private int				  dateMaj;

	/**
	 * @param id
	 * @param libelle
	 * @param version
	 * @param dateMaj
	 */
	protected Entite(String libelle, String version) {
		super();
		this.libelle = libelle;
		this.version = version;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the libelle
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * @param libelle
	 *            the libelle to set
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @return the dateMaj
	 */
	public int getDateMaj() {
		return dateMaj;
	}

	/**
	 * @param dateMaj
	 *            the dateMaj to set
	 */
	public void setDateMaj(int dateMaj) {
		this.dateMaj = dateMaj;
	}

}
