/**
 * 
 */
package fr.aldarion.appli.beans.application;

import java.io.Serializable;
import java.util.List;

import fr.aldarion.appli.beans.Nomenclature;

/**
 * @author cedric
 *
 */
public class Langue implements Serializable {

	/** SERIAL */
	private static final long  serialVersionUID	= 2L;

	/** Menu - Accueil */
	private String			   MENU_ACCUEIL;

	/** Menu - Comment jouer */
	private String			   MENU_COMMENTJOUER;

	/** Menu - Règlement */
	private String			   MENU_REGLEMENT;

	/** Menu - Forum */
	private String			   MENU_FORUM;

	/** Menu - ENREGISTRER */
	private String			   MENU_ENREGISTRER;

	/** Menu - CONNEXION */
	private String			   MENU_CONNEXION;

	/** Slide 1 - NOVUS */
	private String			   SLIDE_1;

	/** Slide 1 - EMPEREUR */
	private String			   SLIDE_2;

	/** Ménu français */
	private List<Nomenclature> menu;
	/** Liste des slides */
	private List<Nomenclature> Slides;

	/** Constructeur sans argument */
	public Langue() {

	}

	/**
	 * @param mENU_ACCUEIL
	 * @param mENU_COMMENTJOUER
	 * @param mENU_REGLEMENT
	 * @param mENU_FORUM
	 * @param mENU_ENREGISTRER
	 * @param mENU_CONNEXION
	 * @param sLIDE_1
	 * @param sLIDE_2
	 * @param menu
	 * @param slides
	 */
	public Langue(String mENU_ACCUEIL, String mENU_COMMENTJOUER, String mENU_REGLEMENT, String mENU_FORUM,
	        String mENU_ENREGISTRER, String mENU_CONNEXION, String sLIDE_1, String sLIDE_2, List<Nomenclature> menu,
	        List<Nomenclature> slides) {
		super();
		MENU_ACCUEIL = mENU_ACCUEIL;
		MENU_COMMENTJOUER = mENU_COMMENTJOUER;
		MENU_REGLEMENT = mENU_REGLEMENT;
		MENU_FORUM = mENU_FORUM;
		MENU_ENREGISTRER = mENU_ENREGISTRER;
		MENU_CONNEXION = mENU_CONNEXION;
		SLIDE_1 = sLIDE_1;
		SLIDE_2 = sLIDE_2;
		this.menu = menu;
		Slides = slides;
	}

	/**
	 * @return the mENU_ACCUEIL
	 */
	public String getMENU_ACCUEIL() {
		return MENU_ACCUEIL;
	}

	/**
	 * @param mENU_ACCUEIL
	 *            the mENU_ACCUEIL to set
	 */
	public void setMENU_ACCUEIL(String mENU_ACCUEIL) {
		MENU_ACCUEIL = mENU_ACCUEIL;
	}

	/**
	 * @return the mENU_COMMENTJOUER
	 */
	public String getMENU_COMMENTJOUER() {
		return MENU_COMMENTJOUER;
	}

	/**
	 * @param mENU_COMMENTJOUER
	 *            the mENU_COMMENTJOUER to set
	 */
	public void setMENU_COMMENTJOUER(String mENU_COMMENTJOUER) {
		MENU_COMMENTJOUER = mENU_COMMENTJOUER;
	}

	/**
	 * @return the mENU_REGLEMENT
	 */
	public String getMENU_REGLEMENT() {
		return MENU_REGLEMENT;
	}

	/**
	 * @param mENU_REGLEMENT
	 *            the mENU_REGLEMENT to set
	 */
	public void setMENU_REGLEMENT(String mENU_REGLEMENT) {
		MENU_REGLEMENT = mENU_REGLEMENT;
	}

	/**
	 * @return the mENU_FORUM
	 */
	public String getMENU_FORUM() {
		return MENU_FORUM;
	}

	/**
	 * @param mENU_FORUM
	 *            the mENU_FORUM to set
	 */
	public void setMENU_FORUM(String mENU_FORUM) {
		MENU_FORUM = mENU_FORUM;
	}

	/**
	 * @return the mENU_ENREGISTRER
	 */
	public String getMENU_ENREGISTRER() {
		return MENU_ENREGISTRER;
	}

	/**
	 * @param mENU_ENREGISTRER
	 *            the mENU_ENREGISTRER to set
	 */
	public void setMENU_ENREGISTRER(String mENU_ENREGISTRER) {
		MENU_ENREGISTRER = mENU_ENREGISTRER;
	}

	/**
	 * @return the mENU_CONNEXION
	 */
	public String getMENU_CONNEXION() {
		return MENU_CONNEXION;
	}

	/**
	 * @param mENU_CONNEXION
	 *            the mENU_CONNEXION to set
	 */
	public void setMENU_CONNEXION(String mENU_CONNEXION) {
		MENU_CONNEXION = mENU_CONNEXION;
	}

	/**
	 * @return the sLIDE_1
	 */
	public String getSLIDE_1() {
		return SLIDE_1;
	}

	/**
	 * @param sLIDE_1
	 *            the sLIDE_1 to set
	 */
	public void setSLIDE_1(String sLIDE_1) {
		SLIDE_1 = sLIDE_1;
	}

	/**
	 * @return the sLIDE_2
	 */
	public String getSLIDE_2() {
		return SLIDE_2;
	}

	/**
	 * @param sLIDE_2
	 *            the sLIDE_2 to set
	 */
	public void setSLIDE_2(String sLIDE_2) {
		SLIDE_2 = sLIDE_2;
	}

	/**
	 * @return the menu
	 */
	public List<Nomenclature> getMenu() {
		return menu;
	}

	/**
	 * @param menu
	 *            the menu to set
	 */
	public void setMenu(List<Nomenclature> menu) {
		this.menu = menu;
	}

	/**
	 * @return the slides
	 */
	public List<Nomenclature> getSlides() {
		return Slides;
	}

	/**
	 * @param slides
	 *            the slides to set
	 */
	public void setSlides(List<Nomenclature> slides) {
		Slides = slides;
	}

}
