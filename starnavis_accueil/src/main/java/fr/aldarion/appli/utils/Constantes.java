package fr.aldarion.appli.utils;

/**
 * 
 * @author cedric
 *
 */
public interface Constantes {

	/** Nom de l'application */
	static final String	APPLI_NOM			 = "StarNavis";

	/** Description de l'application */
	static final String	APPLI_DESC			 = "Jeu sur navigateur - Une nouvelle façon de conquérir l'espace";

	/** Mots clé de référencement */
	static final String	APPLI_KEYWORDS		 = "Jeu, Navigateur, Serveur privé, OGame, Espace";
	/** Version */
	static final String	APPLI_VERSION		 = "1.0";

	/** Flow Controller */
	static final String	APPLI_FLOW_PRINCIPAL = "flux.sn?_flowId=";

	/** Norme : suffix du flow */
	static final String	APPLI_SUFFIX_FLOW	 = "-flow";

	// --------------------------------------------------------------------------------------------------
	// Zone regroupant les liens du site
	// --------------------------------------------------------------------------------------------------

	/** Lien d'accès à l'accueil */
	static String		LIEN_ACCUEIL		 = "accueil.sn";

	/** Lien d'accès à la page comment jouer */
	static String		LIEN_COMMENT_JOUEUR	 = APPLI_FLOW_PRINCIPAL + "commentjouer" + APPLI_SUFFIX_FLOW;

	/** Lien d'accès à la page s'enregistrer */
	static String		LIEN_ENREGISTRER	 = APPLI_FLOW_PRINCIPAL + "enregistrer" + APPLI_SUFFIX_FLOW;

	/** Lien d'accès à la page s'enregistrer */
	static String		LIEN_REGLEMENT		 = APPLI_FLOW_PRINCIPAL + "reglement" + APPLI_SUFFIX_FLOW;

	/** Lien d'accès à la page s'enregistrer */
	static String		LIEN_FORUM			 = APPLI_FLOW_PRINCIPAL + "forum" + APPLI_SUFFIX_FLOW;

	/** Lien d'accès à la page s'enregistrer */
	static String		LIEN_CONNECTER		 = APPLI_FLOW_PRINCIPAL + "connecter" + APPLI_SUFFIX_FLOW;

	// --------------------------------------------------------------------------------------------------
	// Zone regroupant les images du site
	// --------------------------------------------------------------------------------------------------

	/** Slide 1 */
	static String		IMAGE_SLIDE_1		 = "/slide/slide_1.jpg";

	/** Slide 2 */
	static String		IMAGE_SLIDE_2		 = "/slide/slide_2.jpg";

}
