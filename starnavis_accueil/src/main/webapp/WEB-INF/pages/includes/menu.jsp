<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<div id="menu">  
	<div id="navigation"> 		
			<ul>
				<c:forEach items="${accueilform.elementsMenu}" var="ligne">
					<li><a title="${ligne.value}" href="<c:url value="${ligne.id}" />">${ligne.value} </a> </li>
				</c:forEach>
			</ul>			
	</div>
</div>