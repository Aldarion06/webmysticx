<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

	<head>
		<meta charset="UTF-8">
  		<meta name="description" 	content="${application.description}">
  		<meta name="keywords" 		content="${application.motCles}">
  		<meta name="author" 		content="Aldarion">
  		<meta name="viewport" 		content="width=device-width, initial-scale=1.0">
  		<link rel="shortcut icon" 	href="<c:url value="/resources/theme1/img/favicon.ico" />" type="image/x-icon" />
  		
  		
		<title> ${titrepage} </title>
		<link href="<c:url value="${src}css/style.css" />" 	rel="stylesheet">
		<link href="<c:url value="${src}css/loopslider.css" />" rel="stylesheet">
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js" ></script>
		<script src="<c:url value="${src}js/jquery.loopslider.js" />" ></script>
		<script src="<c:url value="${src}js/application.js" />" ></script>
		
		<script type="text/javascript">
			// Script de récupération du language de l'utilisateur
			recupererLanguageUtilisateur();
		</script>
	</head>