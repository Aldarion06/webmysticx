<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!-- Initialisation des variables -->
<c:set var="titrepage" 		value="${accueilform.titrePage }" 	scope="request" />
<c:set var="application" 	value="${accueilform.application }" scope="request" />
<c:set var="src"			value="/resources/theme1/"			scope="request" />
<c:set var="src_img"		value="${src}/img/"					scope="request" />

<html>	
	<c:import url="includes/head.jsp" />	
	<body>	
	<c:import url="includes/menu.jsp" />
		
		<div id="contenu">									
			<div id="corp">
				<div class="slider-container">
				<c:forEach items="${accueilform.slides}" var="ligne">
					<figure>
						<img alt="<c:url value="${src_img}${ligne.id}" />" src="<c:url value="${src_img}${ligne.id}" />">
						<figcaption>${ligne.value}</figcaption>
					</figure>	
				</c:forEach>								
				</div>			
			</div>
		</div>
	</body>
	<script>
	$(function(){
		$('.slider-container').loopslider({
			autoplay: true
			,visibleItems: 1
			,step: 1
			,pagination: false
		});
	});</script>
</html>
