	/**
 * Application
 * @version 0.1
 * @author Aldarion

 */


function recupererLanguageUtilisateur()
{
	
	var url = new URL(document.URL);
	var langue = url.searchParams.get("langue");
	
	// On effectue ce controle si langage == null	
	if(langue == null) {
		
		// Récupération du langage
		if (navigator.browserLanguage) {
			var language = navigator.browserLanguage;
		} else {
			var language = navigator.language;
		}
		
		// On alimente l'attribut en fonction du langage
		if (language.indexOf('fr') > -1) document.location.href = 'accueil.sn?langue=fr';
		if (language.indexOf('en') > -1) document.location.href = 'accueil.sn?langue=en';
		
	}
	
}
